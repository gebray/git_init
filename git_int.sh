#!/usr/bin/env bash
git init
clear
printf "%sGIT\nconfig --global --add user.name\nEnter your name: "
read username
while true; do
	if [[ -z "$username" ]];then
		printf "%sNot entered username for git config.\nEnter username: "
		read username
	else
		git config --global user.name "$username"
		break
	fi
done
printf "%sconfig --global --add user.email\nEnter your email: "
read email
while true; do
	if [[ -z "$email" ]];then
		printf "%sNot entered email for git config.\nEnter email: "
		read email
	else
		git config --global user.email "$email"
		break
	fi
done
clear
printf "%sGIT\nEnter the project name: "
read name
while true; do
	if [[ -z "$name" ]];then
		printf "%sNot entered project name.\nEnter project name: "
		read name
	else
		git remote add origin https://gitlab.com/abebegebray/$name.git
		break
	fi
done
git add .
git commit -m "update"
git push -u origin master



